
require 'httparty'
require 'rspec'
require 'pry'
require 'json'
require 'yaml'
require 'colorize'
require 'faker'
require 'open-uri'
require 'date'
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'rails'
require 'site_prism'
require 'csv'
require 'appium_lib'
require 'tiny_tds'
require 'report_builder'
require 'os'
require 'jira-ruby'
require "cpf_cnpj"
require 'roo'

Pry.config.color = true
# Environment definitions

AMB = ENV['AMB'] || 'hom'
HEADLESS = ENV['HL'] || "false"
MOBILE = ENV['MOBILE'] || 'false'
PLATFORM_NAME = ENV['PLATFORM_NAME'] || 'false'

BASE_URL = YAML.load_file('./config/environment.yml')[AMB]