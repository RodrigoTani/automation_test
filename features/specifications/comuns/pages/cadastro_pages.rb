class RegisterPages
    include Capybara::DSL

    def generate_code(number)
        charset = Array('A'..'Z') + Array('a'..'z') + Array(0..9)
        Array.new(number) { charset.sample }.join
    end

    def create_email(email)
        assert_selector("#email_create", wait: 120)
    
        if email.blank?
            email = "#{Faker::Name.first_name}.#{Faker::Name.last_name}@test.com"
        end
        $email = email

        find("#email_create", wait: 10).set($email)
        find("#SubmitCreate", wait: 10).click

        if page.has_css?("#create_account_error")
            raise "Usuario ja cadastrado! Favor escolher outro e-mail\n"
        end
    end

    def personal_information
        assert_selector("#customer_firstname", wait: 120)

        find("#uniform-id_gender1").click
        find("#customer_firstname").set("Rodrigo")
        find("#customer_lastname").set("Tani")
        
        $senha = generate_code(5)
        find("#passwd", wait: 10).set($senha)

        find("#uniform-days", wait: 10).all('option', text:2)[0].click
        find("#uniform-months", wait: 10).select("August").click
        find("#uniform-years", wait: 10).select("1997").click

        all("label[for='optin']")[0].click
    end

    def address_information
        find("#firstname", wait: 10).set("Rodrigo")
        find("#lastname", wait: 10).set("Tani")
        find("#company", wait: 10).set("Tani It consulting")
        find("#address1", wait: 10).set("Avenida das Nações Unidas")
        find("#city", wait: 10).set("São Paulo")

        find("#uniform-id_state", wait: 10).select('Alabama').click
        find("#postcode", wait: 10).set("00000")
        find("#phone_mobile", wait: 10).set("11963925158")
        
        find("#alias", wait: 10).native.clear
        find("#alias", wait: 10).set("12901 North Tower - 3o floor")
    end

    def submit
        find("#submitAccount", wait: 10).click

        assert_selector(".navigation_page", wait: 120)

        if all("span[class='navigation_page']")[0].text != "My account"
            raise "Não está na pagina do My account"
        end
    end
end