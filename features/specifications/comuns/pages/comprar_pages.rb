class ComprarPages
    include Capybara::DSL

    def add_vestido(cor, tamanho)
        case cor 
        when "Pink"
            find("#color_24", wait:10).click
        when "Beige"
            find("#color_7", wait:10).click
        end
        preco = find("#our_price_display").text
        
        vestido = ""
        vestido = "#{cor}/#{tamanho}/#{preco}"

        find("#uniform-group_1", wait:10).find('option', exact_text: tamanho).click
        find("#add_to_cart", wait:10).click

        all("span[class='continue btn btn-default button exclusive-medium']", wait:10)[0].click
        return vestido
    end

    def choose_dress
        assert_selector(".category-name", wait: 30)
        all(:link,'Printed Dress')[1].click
        assert_selector("#product_reference", wait: 30)
        $lista_vestidos = []
    end

    def add_dress_in_cart(cor, tamanho)
        vestido = add_vestido(cor, tamanho)
        $lista_vestidos << vestido
    end

    def validate_cart
        $dados["ListaVestidos"] = $lista_vestidos
        click_link 'cart'
        assert_selector("#cart_title", wait: 30) 

        for qtd_vestidos in 0...$dados["ListaVestidos"].count
            dados = $dados["ListaVestidos"][qtd_vestidos].split("/")

            within all("tbody")[0].all("tr")[qtd_vestidos] do
            #Valida cor
                raise "Cor inconsistente" if !all(".cart_description")[0].text.include?(dados[0])
            #Valida Tamanho
                raise "Tamanho inconsistente" if !all(".cart_description")[0].text.include?(dados[1])
            #valida Preco
                raise "Preco inconsistente" if !all('.price')[0].text.include?(dados[2])
            end
        end

        click_link_or_button 'Proceed to checkout'
    end

    def validate_address
        assert_selector(".page-heading", wait: 30) 

        click_link_or_button 'Proceed to checkout'
    end

    def accept_terms
        assert_selector(".page-heading", wait: 30) 

        find("#uniform-cgv").click
        click_link_or_button 'Proceed to checkout'
    end

    def set_paymment(formapagamento)
        assert_selector(".page-heading", wait: 30) 

        if formapagamento == "Check"
            find('.cheque').click
        elsif formapagamento == "Bank"
            find('.bankwire').click
        else
            raise "Não existe esta forma de pagamento!"
        end
        $dados.merge!("FormaPagamento" => formapagamento)
    end

    def confirm_buy
        assert_selector(".page-heading", wait: 30)
        click_link_or_button 'I confirm my order'
    end

    def validate_order(string)
        click_link_or_button 'Back to orders'
        assert_selector(".page-heading", wait: 30)

        preco_total = all('.price')[0].text
        metodo_pagamento = find('.history_method').text

        raise "Forma de pagamento incosistente" if !metodo_pagamento.include?($dados["FormaPagamento"].downcase)

        click_link 'Details'
    end
end