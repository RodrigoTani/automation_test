Dado('que eu esteja logado no sistema') do
    visit("http://automationpractice.com/")

    find(".login").click
    assert_selector("#email_create", wait: 30)

    if $email.blank?
        $email = "Carlos.Mayer@test.com"
    end
    if $senha.blank?
        $senha = "ry6ms"
    end
    find("#email").set($email)
    find("#passwd").set($senha)
    find("#SubmitLogin").click
    assert_selector(".navigation_page", wait: 30)

    @comprar = ComprarPages.new    
end

Quando('acesso o menu {string}, submenu {string}') do |menu, submenu|
    click_link menu 
    first(:link, submenu).click 
end

Dado('escolho o vestido') do
    @comprar.choose_dress
end

Quando('adiciono um vestido da cor {string}, tamanho {string}') do |cor, tamanho|
    @comprar.add_dress_in_cart(cor, tamanho)
end

Dado('valido o carrinho') do
    @comprar.validate_cart
end

Dado('valido o endereco') do
    @comprar.validate_address
end

Quando('aceito os termos do servico') do
    @comprar.accept_terms
end

Quando('utilizo a forma de pagamento {string}') do |formapagamento|
    @comprar.set_paymment(formapagamento)
end

Entao('finalizo a compra') do
    @comprar.confirm_buy
end

Entao('valido os vestidos em {string}') do |string|
    @comprar.validate_order(string)
end