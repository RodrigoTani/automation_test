Dado('que eu acesse o link') do
    visit("http://automationpractice.com/")

    find(".login", wait: 10).click

    @register = RegisterPages.new
end

Dado('preencho o novo {string}') do |email|
    @register.create_email(email)
end

Dado('preencho as informacoes pessoais') do
    @register.personal_information
end

Dado('preencho o endereco') do
    @register.address_information
end

Entao('finalizo o cadastro') do
    @register.submit
end